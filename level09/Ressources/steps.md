## Level 09

### First step

After searching a bit on what we had to do and trying to understand what was binary's job,  
thinking that he had something to do with the file token, we realised it was just a binary that changed his output.

```bash
$> ./level09 somestuff
spohwy{mn
...
$> ./level09 a
a
$> ./level09 b
b
$> ./level09 abc
ace
$> ./level09 acb
add
```

It seems it just add, for each char, char's index to initial ascii value.  
We just made a python script that substract for each char his index in the string.

```bash
$> python /tmp/test.py `cat token`
f3iji1ju5yuevaus41q1afiuq

$> su flag09
Password:
Don t forget to launch getflag !
$> getflag
Check flag.Here is your token : s5cAJpM8ev6XHw998pRWG728z
```