import sys

try:
    print(
        "".join(
            chr(ord(char) - idx) for idx, char in enumerate(sys.argv[1])
        )
    )
except IndexError:
    print("Usage: python decrypt.py [key]")