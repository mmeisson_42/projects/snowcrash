## Level 14

### First step

There is no clue at this level, we must then take into consideration what we did in the previous level. Check for some uid or similar bypass. 

### Second step

We can first see that all uid, gid, etc.. for this flag are equal to 3014.  

```bash
$> id flag14
uid=3014(flag14) gid=3014(flag14) groups=3014(flag14),1001(flag)
```

The only executable we can look at is */bin/getflag*.

```bash
$> strings /bin/getflag
...
stderr
getuid
ptrace
...
```

Interesting thing, it calls *getuid* as *level13* did and compares it to 0xbbe which is 3006 in decimal while our current uid should be 3014.

By trying to do the same manipulation as *level13* using *gdb* we get this error:

```bash
$> gdb level14
(gdb) set disassembly-flavor intel
(gdb) disassemble main
...
0x08048afd <+439>:  call 0x80484b0 <getuid@plt>
0x08048b02 <+444>:  mov  DWORD PTR [esp+0x18],eax
0x08048b06 <+448>:  mov  eax,DWORD PTR [esp+0x18]
0x08048b0a <+452>:  cmp  eax,0xbbe
...
(gdb) break *0x08048b0a
Breakpoint 1 at 0x8048b0a
(gdb) run
Starting program: /bin/getflag
You should not reverse this
```

By winding our way up to find out where this error comes from, we realized that *ptrace* was the turning point. Before its call we can set a break point but not after it returns.  
We can also guess that it is cause by a *test* done with the return value.  

By checking its return value:  

```bash
...
0x08048989 <+67>:   call 0x8048540 <ptrace@plt>
0x0804898e <+72>:   test eax,eax
...
(gdb) break *0x0804898e
...
(gdb) run
...
(gdb) print $eax
$1 = -1
```

*ptrace* returns *-1*, which is an error.

We can also see the condition by using *ltrace*.  

```bash
$> ltrace /bin/getflag
__libc_start_main(0x8048946, 1, 0xbffff7c4, 0x8048ed0, 0x8048f40 <unfinished ...>
ptrace(0, 0, 1, 0, 0)                                                                                    = -1
puts("You should not reverse this"You should not reverse this
)
```

### Final step

Let's try to replace *ptrace* returned value by a valid one so that we can change *getuid* return's value later on in the program without getting an error.  

```bash
(gdb) break *0x0804898e
Breakpoint 1 at 0x804898e
(gdb) break *0x08048b0a
Breakpoint 2 at 0x8048b0a
(gdb) run
...
(gdb) set $eax=1
(gdb) continue
...
(gdb) set $eax=3014
(gdb) continue
Continuing.
Check flag.Here is your token : 7QiHafiNa3HVozsaXkawuYrTstxbpABHD8CPnHJ
```

After bypassing *ptrace*, we do not get an error anymore. We can then apply the same bypass than previously by setting *eax* to our current *uid* **3014**.  

We obtained the password to flag14

```bash
Congratulation. Type getflag to get the key and send it to me the owner of this livecd :)
flag14@SnowCrash:~$ getflag
Check flag.Here is your token : 7QiHafiNa3HVozsaXkawuYrTstxbpABHD8CPnHJ
```