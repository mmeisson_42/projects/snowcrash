## Level 13

### First step

By executing the *level13* binary, we get as output:

```bash
$> ./level13
UID 2013 started us but we we expect 4242
```

We now know the goal of the level, which is: somehow get a way to force UID to be 4242.

### Second step

Using *gdb*, let's have a look at the assembly code where we can see 2 interesting things.

```bash
$> gdb level13
(gdb) set disassembly-flavor intel
(gdb) disassemble main
    ...
    0x08048595 <+9>: call 0x8048380 <getuid@plt>
    0x0804859a <+14>: cmp eax,0x1092
    ...
```

Firstly there is a call made to getuid, then there is a comparaison made between *eax* register who contains the value returned by *getuid* and an hexadecimal value *0x1092* which is equal to *4242* in decimal.  
By setting a breakpoint to set *eax* value to 4242 we can make the comparaison succeed and then access to the token.  
```bash
(gdb) break *0x0804859a
...
(gdb) run
(gdb) set $eax=0x1092
...
(gdb) continue
Continuing.
your token is 2A31L79asukciNyi8uppkEuSx
```
