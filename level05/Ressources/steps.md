## Level 05

### First step

> You have received a mail

After checking /var/log and then /var/mail, we saw a crontab in the file /level05.
The script executed by the crontab checked in a dir where we had write access. So We just had to put a getflag in

```bash
echo 'getflag > /opt/openarenaserver/flag05' > /opt/openarenaserver/test
```

When the file flag05 appeared, it contained the required flag
> Check flag.Here is your token : viuaaale9huek52boumoomioc