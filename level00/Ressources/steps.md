## Level 00

### First step
```bash
$> find / -user "flag00" 2>&-
/usr/sbin/john
/rofs/usr/sbin/john
$> cat /usr/sbin/john
cdiiddwpgswtgt
```

### Second step

- Go to: https://www.dcode.fr/chiffre-cesar
- Then, decode the previously found string
- Choose the most humanly readable result, which is: **nottoohardhere**
